const Router = require("koa-router");
const jwt = require("jsonwebtoken");

const login = new Router({
	prefix: "/api/login"
});

const { User } = require("../models/index");
const { generateAccessToken, generateRefreshToken } = require("../auth/config");

login.get("/", async ctx => (ctx.body = "GET /login"));
login.post("/", async ctx => {
	ctx.body = "POST /login";
	try {
		const { email, password } = ctx.request.body;
		console.log("/login ctx.request.body", ctx.request.body);
		const user = await User.findOne({
			where: {
				email,
				password
			}
		});
		if (user) {
			ctx.status = 200;
			ctx.response.body = {
				accessToken: generateAccessToken(user.get().id),
				refreshToken: generateRefreshToken(user.get().id)
			};
		} else {
			ctx.status = 404;
			ctx.body = "No matching user found";
		}
	} catch (err) {
		console.log("An unexpected error occured", err);
	}
});

module.exports = login;
