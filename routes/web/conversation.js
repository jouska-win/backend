const Router = require("koa-router");
const fs = require("fs");

const { authenticate } = require("../../auth/config");

const router = new Router({
	prefix: "/api/web"
});

const { User, Conversation, Image } = require("../../models/index");

router.get("/view/:id", authenticate, async ctx => {
	try {
		const conversationId = ctx.params.id;
		const conversation = await Conversation.findOne({
			where: {
				id: conversationId
			}
		});
		if (conversation) {
			ctx.status = 200;
			ctx.body = conversation;
		} else {
			ctx.status = 404;
			ctx.body = "No such conversation found";
		}
	} catch (err) {
		console.log("\n weird eror at /api/web/view:id");
	}
});

router.post("/edit", authenticate, async ctx => {
	const { id, title, description, text, tags } = ctx.request.body;
	try {
		const conversation = await Conversation.update(
			{
				title,
				description,
				text
				// tags
			},
			{
				where: {
					id
				}
			}
		);
		if (conversation) {
			ctx.status = 200;
			ctx.body = "Conversation successfully updated";
		} else {
			ctx.status = 404;
			ctx.body = "No such conversation found";
		}
	} catch (err) {
		console.log("error in post /api/web/edit", err);
	}
});

router.put("/create", authenticate, async ctx => {
	// TODO: cleanup below logic
	try {
		const { id } = ctx.accessTokenData;
		const {
			title,
			description,
			text,
			tags,
			base64Image,
			imageName
		} = ctx.request.body;
		if (!title || !description || !text) {
			ctx.status = 400;
			ctx.body =
				"Every conversation needs to have a title, description and actual text";
		} else {
			const conversation = await Conversation.create({
				userId: id,
				title,
				description,
				text,
				tags: ["test", "test2"],
				imageName
			});
			if (conversation) {
				// conversation created, now it's time to save the image

				const user = await User.findOne({
					where: {
						id
					}
				});
				const folderName = user.get().folderName;
				const imagePath = `${process.env.PWD}/images/${folderName}`;

				// First insert into image table
				// const image = await Image.create({
				// 	id: conversation.get().id,
				// 	userId: user.get().id,
				// 	imageName
				// });

				try {
					// Check if user flder exists
					fs.openSync(`${imagePath}`);
				} catch (err) {
					// if not create it
					fs.mkdirSync(`${imagePath}`);
				}

				// TODO: minify image
				// remove unneeded information like data:image/jpeg;base64,
				const base64Data = base64Image.replace(
					/^data:([A-Za-z-+\/]+);base64,/,
					""
				);

				// Second save image to user folder
				const buffer = Buffer.from(base64Data, "base64");
				fs.writeFileSync(`${imagePath}/${imageName}`, buffer);

				ctx.status = 200;
				ctx.body = "Successfully created a conversation";
			} else {
				ctx.status = 500;
				ctx.body = "Error while trying to save your conversation";
			}
		}
	} catch (err) {
		console.log("Error in  PUT /api/web/create", err);
	}
});

router.delete("/delete/:id", authenticate, async ctx => {
	const id = ctx.params.id;
	console.log("\n\n/delete/id is", Object.keys(ctx.params.id));
	// Object.keys(ctx.params)
	try {
		const conversation = await Conversation.destroy({
			where: {
				id
			}
		});
		if (conversation) {
			ctx.status = 200;
			ctx.body = "Conversation successfully deleted";
		} else {
			ctx.status = 500;
			ctx.body = "Unable to delete conversation O.O";
		}
	} catch (err) {
		console.log("error in post /api/web/edit", err);
	}
});

module.exports = router;
