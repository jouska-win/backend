const Router = require("koa-router");
const fs = require("fs");

const { User, Conversation } = require("../../models/index");
const { authenticate, getAccessTokenData } = require("../../auth/config");

const router = new Router({
	prefix: "/api/web"
});

router.get("/", authenticate, async ctx => {
	try {
		const { id } = ctx.accessTokenData;
		// ctx.body = "GET /api/web";
		// Get conversations
		const conversations = await Conversation.findAll({
			where: {
				userId: id
			},
			include: [
				{
					model: User,
					as: "user",
					where: {
						id
					},
					attributes: ["folderName"]
				}
			]
		});

		// TODO: add an image to each conversations
		const data = conversations.map(c => {
			const cData = c.get();
			const imageName = cData.imageName;
			const folderName = cData.user.get().folderName;

			// console.log("\n\n imageName", imageName, "\n\nfolderName", folderName);
			const image = fs.readFileSync(
				`${process.env.PWD}/images/${folderName}/${imageName}`,
				"base64"
			);
			return {
				...cData,
				image
			};
		});

		// console.log("\n\ndata", data);

		ctx.status = 200;
		ctx.body = data;
	} catch (err) {
		ctx.status = 500;
		console.log("Unexpected error at GET /api/web", err);
	}
});
router.post("/", async ctx => (ctx.body = "POST /api/web"));

module.exports = router;
