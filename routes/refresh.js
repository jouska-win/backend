const Router = require("koa-router");

const refresh = new Router({
	prefix: "/api/refresh"
});

const { generateAccessToken, verifyToken } = require("../auth/config");

refresh.get("/", async ctx => (ctx.body = "GET /refresh"));
refresh.post("/", async ctx => {
	const refreshToken = ctx.request.body.Authorization;

	try {
		// verify access-token from cookies
		const refreshTokenData = verifyToken(refreshToken);
		ctx.status = 200;
		ctx.body = generateAccessToken(refreshTokenData.id);
	} catch (err) {
		// Expired - create and return new
		if (err.name === "TokenExpiredError") {
			// read refresh-token from request.body.Authorization
			ctx.status = 200;
			const refreshTokenData = verifyToken(refreshToken);
			console.log("refreshTokenData", refreshTokenData);
			ctx.body = generateAccessToken(refreshTokenData.id);
			console.log(
				"Access-token was expired but refresh-token was not so I created a new access-token"
			);
		} else {
			console.log("An unexpected error occured while trying to LOGIIIN", err);
			ctx.body = 500;
			ctx.body = "An error occured while trying to refresh your access-token";
		}
	}
});

module.exports = refresh;
