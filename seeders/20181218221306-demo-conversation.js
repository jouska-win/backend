"use strict";

module.exports = {
	up: (queryInterface, Sequelize) => {
		/*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
		return queryInterface.bulkInsert(
			"Conversations",
			[
				{
					id: 1,
					userId: 1,
					title: "Demo conversation title",
					description: "Demo conversation description",
					text: "This is an example of some lorm ipsum text lalalalala",
					tags: ["test", "true"],
					createdAt: new Date(),
					updatedAt: new Date()
				}
			],
			{}
		);
	},

	down: (queryInterface, Sequelize) => {
		/*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
		return queryInterface.bulkDelete("Conversations", null, {});
	}
};
