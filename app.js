const Koa = require("koa");
const Router = require("koa-router");
const bodyParser = require("koa-bodyparser");
const session = require("koa-session");
const shortId = require("shortid");

require("dotenv").config();
const signup = require("./routes/signup");
const login = require("./routes/login");
const refresh = require("./routes/refresh");
const indexWeb = require("./routes/web/index");
const loginWeb = require("./routes/web/login");
const refreshWeb = require("./routes/web/refresh");
const conversationWeb = require("./routes/web/conversation");

const app = new Koa();
const router = new Router();

// Sessions
app.use(session({}, app));

app.use(bodyParser());

app.use(signup.routes(), signup.allowedMethods());
app.use(login.routes(), login.allowedMethods());
app.use(refresh.routes(), refresh.allowedMethods());
app.use(indexWeb.routes(), indexWeb.allowedMethods());
app.use(loginWeb.routes(), loginWeb.allowedMethods());
app.use(refreshWeb.routes(), refreshWeb.allowedMethods());
app.use(conversationWeb.routes(), conversationWeb.allowedMethods());

app.use(async ctx => (ctx.body = ctx.request));

app.listen(3001, () => console.log("Server started on port 3001"));
