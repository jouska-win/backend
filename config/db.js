const Sequelize = require("sequelize");

const { DB_NAME, DB_USER, DB_PASSWORD, DB_DIALECT } = process.env;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASSWORD, {
	host: DB_HOST,
	dialect: DB_DIALECT
});

sequelize
	.authenticate()
	.then(() =>
		console.log("Connection to the Database has been established successfully")
	)
	.catch(err => console.error("Unable to connect to the database: ", err));

module.exports = sequelize;
