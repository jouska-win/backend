const jwt = require("jsonwebtoken");
const { JWT_SECRET } = process.env;

const secret = JWT_SECRET;

const generateAccessToken = id => {
	const token = {
		token: jwt.sign({ id }, secret, {
			expiresIn: 60 * 30 // 30 min
		})
	};
	return token;
};

const generateRefreshToken = id => {
	const token = {
		token: jwt.sign({ id }, secret, {
			expiresIn: 60 * 60 * 24 * 7 // 1 week
		})
	};
	return token;
};

const verifyToken = token => {
	return jwt.verify(token, secret);
};

// Only for the web
const refreshAccessToken = cookies => {
	try {
		const refreshToken = cookies.get("refresh-token");
		const refreshTokenData = verifyToken(refreshToken);
		const accessToken = generateAccessToken(refreshTokenData.id);
		cookies.set("access-token", accessToken.token);
	} catch (err) {
		if (
			err.name === "JsonWebTokenError" &&
			err.message === "jwt must be provided"
		) {
			ctx.status = 403;
			ctx.body = "You need an access token to access this page";
		}
	}
};

const authenticate = async (ctx, next) => {
	try {
		const accessToken = ctx.cookies.get("access-token");
		ctx.accessTokenData = verifyToken(accessToken);
		await next();
	} catch (err) {
		if (
			err.name === "JsonWebTokenError" &&
			err.message === "jwt must be provided"
		) {
			ctx.status = 403;
			ctx.body = "You need an access token to access this page";
		} else if (err.name === "TokenExpiredError" || err.name === "jwt expired") {
			refreshAccessToken(ctx.cookies);
			const accessToken = ctx.cookies.get("access-token");
			try {
				ctx.accessTokenData = verifyToken(accessToken);
			} catch (err) {
				console.log("Internal SERVER error", err);
			}
			await next();
		}
	}
};

const getAccessTokenData = cookies => {
	try {
		const accessToken = cookies.get("access-token");
		const accessTokenData = verifyToken(accessToken);
		return accessTokenData;
	} catch (err) {
		if (err.name === "TokenExpiredError") {
			refreshAccessToken(cookies);
			const accessToken = cookies.get("access-token");
			const accessTokenData = verifyToken(accessToken);
			return accessTokenData;
		} else {
		}
	}
};

module.exports = {
	generateAccessToken,
	generateRefreshToken,
	verifyToken,
	refreshAccessToken,
	authenticate,
	getAccessTokenData
};
